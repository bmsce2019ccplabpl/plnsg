#include <stdio.h>

int main()
{
char a;
FILE *fp;
fp=fopen("input.dat","w");
printf("Enter the contents to write in the file\n");
while((a=getchar())!=EOF)
{
    fputc(a,fp);
}
fclose(fp);
fp=fopen("input.dat","r");
printf("The contents of the file are:\n");
while((a=fgetc(fp))!=EOF)
{
    printf("%c",a);
}
fclose(fp);

}