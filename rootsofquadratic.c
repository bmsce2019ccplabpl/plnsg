#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,d,x1,x2,i;
    printf("Enter the values of a, b and c of the equation (ax^2 + bx + c) :\n");
    scanf("%f%f%f",&a,&b,&c);
    d = (b*b) - (4*a*c);
    switch(d > 0)
    {
        case 1:
           x1 = (-b + sqrt(d))/2*a;
           x2 = (-b - sqrt(d))/2*a;
           printf("The 2 real roots of the equation are %.2f and %.2f\n",x1,x2);
        break;
        
        case 0:
          switch(d < 0)
          {
              case 1:
                 x1 = x2 = (-b / (2*a));
                 i = sqrt(-d)/ (2*a);
                 printf("The 2 complex roots of the equation are %.2f + i%2f and %.2f + i%.2f\n",x1,i,x2,i);
              break;
              
              case 0:
                x1 = x2 = -b / (2*a);
                printf("The 2 real roots of the equation are %.2f and %.2f\n",x1,x2);
              break;
          }
    }
    return 0;
}